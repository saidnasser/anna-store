$('.card.product-item').on('mouseenter', function (event) {
    console.log(window.innerWidth)
    if (window.innerWidth < 768) {
        return false;
    }
    $(this).children('.card-img').children('.overlay').fadeIn(300)
    $(this).children('.card-img').children('.img').children('img:first').css('opacity', 0)
    $(this).children('.card-img').children('.img').children('img:last').css('opacity', 1)
    // $(this).children('.card-body').css('background', '#131619').animate(300)
    $(this).children('.card-body').children('.card-text').children('.rating').fadeIn(300)
});
$('.card').on('mouseleave', function (event) {
    if (window.innerWidth < 768) {
        return false;
    }
    $(this).children('.card-img').children('.overlay').fadeOut(300)
    $(this).children('.card-img').children('.img').children('img:last').css('opacity', 0)
    $(this).children('.card-img').children('.img').children('img:first').css('opacity', 1)
    // $(this).children('.card-body').css('background', '#0d0f11').animate(300)
    $(this).children('.card-body').children('.card-text').children('.rating').fadeOut(300)
});
$('input[type="search"]').on('focus', function (event) {
    console.log(getActiveStyleSheet());
    if (getActiveStyleSheet() === 'black-theme') {
        $(this).parent().css('border-color', '#D10014')
    } else {
        $(this).parent().css('border-color', '#bcac76')
    }
})
$('input[type="search"]').focusout(function (event) {
    $(this).parent().css('border-color', 'transparent')
})




/** animate navbar on scroll
 * ==========================
 */
$(function () {
    if (document.querySelector('header.main-header.home-header')) {
        $('header.main-header.home-header #mynavbar').on('show.bs.collapse', function () {
            document.querySelector('header.main-header.home-header').classList.add('animate-nav')
        })
        $('header.main-header.home-header #mynavbar').on('hidden.bs.collapse', function () {
            document.querySelector('header.main-header.home-header').classList.remove('animate-nav')
        })
    }
})




if (document.querySelector('#quickView')) {

    var quickViewSlider = {};
    $('#quickView').on('shown.bs.modal', function (e) {
        quickViewSlider = new Swiper('#quickViewSlider', {
            mode: 'horizontal',
            loop: true,
            on: {
                slideChangeTransitionStart: highlightThumb
            }
        });
    })

    $('#quickView').on('hidden.bs.modal', function (e) {
        quickViewSlider.destroy();
    })


}
if (document.querySelector('#viewSlider')) {
    var viewSlider = new Swiper('#viewSlider', {
        mode: 'horizontal',
        loop: true,
        on: {
            slideChangeTransitionStart: highlightThumb
        }
    });
}
function highlightThumb() {
    var sliderId = this.$el.attr('id');
    var thumbs = $('.swiper-thumbs[data-swiper="#' + sliderId + '"]')
    // if thumbs for this slider exist
    if (thumbs.length > 0) {
        thumbs.find(".swiper-thumb-item.active").removeClass('active');
        thumbs.find(".swiper-thumb-item").eq(this.realIndex).addClass('active');
    }
}

$('.swiper-thumb-item').on('click', function (e) {
    e.preventDefault();
    var swiperId = $(this).parents('.swiper-thumbs').data('swiper');
    $(swiperId)[0].swiper.slideToLoop($(this).index())
});



$(function () {

    if (document.querySelector('#product-details')) {
        var productDetailsSlider = new Swiper('#product-details', {
            mode: 'horizontal',
            loop: true,
            on: {
                slideChangeTransitionStart: highlightThumb
            }
        });


        $('.swiper-thumb-item').on('click', function (e) {
            e.preventDefault();
            var swiperId = $(this).parents('.swiper-thumbs').data('swiper');
            $(swiperId)[0].swiper.slideToLoop($(this).index())
        });
    }
    function highlightThumb() {
        var sliderId = this.$el.attr('id');
        var thumbs = $('.swiper-thumbs[data-swiper="#' + sliderId + '"]')
        // if thumbs for this slider exist
        if (thumbs.length > 0) {
            thumbs.find(".swiper-thumb-item.active").removeClass('active');
            thumbs.find(".swiper-thumb-item").eq(this.realIndex).addClass('active');
        }
    }
})









if (typeof new Swiper() === 'object') {
    var headerSlider = new Swiper('#header-swiper', {
        direction: "horizontal",
        slidesPerView: 1,
        spaceBetween: 0,
        centeredSlides: true,
        loop: true,
        speed: 1500,
        parallax: true,
        autoplay: {
            delay: 5000,
        },
        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            dynamicBullets: true
        },
        // Navigation arrows
        // navigation: {
        //     nextEl: '#swiper-button-next',
        //     prevEl: '#swiper-button-prev',
        // },
    });
    var brandsSlider = new Swiper('#brands-swiper', {
        slidesPerView: 6,
        spaceBetween: 15,
        // loop: true,
        roundLengths: true,
        breakpoints: {
            1200: {
                slidesPerView: 5
            },
            991: {
                slidesPerView: 5
            },
            768: {
                slidesPerView: 4
            },
            578: {
                slidesPerView: 3
            },
            0: {
                slidesPerView: 2
            }
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            dynamicBullets: true
        },
    });
    var youMightAlsoLikeSlider = new Swiper('#you-might-also-like-swiper', {
        slidesPerView: 6,
        spaceBetween: 10,
        loop: true,
        roundLengths: true,
        breakpoints: {
            1200: {
                slidesPerView: 5
            },
            991: {
                slidesPerView: 5
            },
            768: {
                slidesPerView: 4
            },
            578: {
                slidesPerView: 3
            },
            0: {
                slidesPerView: 1
            }
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            dynamicBullets: true
        },
    });



}

if (document.querySelector('#range')) {
    var range = document.querySelector('#range');
    noUiSlider.create(range, {

        range: {
            'min': 40,
            'max': 400
        },

        step: 10,

        // Handles start at ...
        start: [40, 400],

        // ... must be at least 300 apart
        margin: 50,

        // ... but no more than 600
        limit: 400,

        // Display colored bars between handles
        connect: true,

        // Put '0' at the bottom of the slider
        direction: 'ltr',
        orientation: 'horizontal',

        // Move handle on tap, bars are draggable
        behaviour: 'tap-drag',
        tooltips: true

        // Show a scale with the slider
        // pips: {
        //     mode: 'steps',
        //     stepped: false,
        //     density: 4
        // }
    });
}


$('.increase').on('click', function (event) {
    console.log($(this).siblings('.amount-field').val())
    $(this).siblings('.amount-field').val(
        `${(Number($(this).siblings('.amount-field').val()) + 1) <= 20 ? Number($(this).siblings('.amount-field').val()) + 1 : Number($(this).siblings('.amount-field').val())}`
    )
})
$('.decrease').on('click', function (event) {
    console.log('sssssss')
    $(this).siblings('.amount-field').val(
        `${(Number($(this).siblings('.amount-field').val()) - 1) > 0 ? Number($(this).siblings('.amount-field').val()) - 1 : Number($(this).siblings('.amount-field').val())}`
    )
})





/** ==== count down ==== */
$(function () {
    if (document.querySelector('#count-down')) {
        $('#days').text('14');
        $('#hours').text('18');
        $('#minutes').text('37');
        $('#seconds').text('40');
        console.log(typeof $('#seconds').text());
        var countDown = setInterval(() => {
            // seconds
            if ((Number($('#seconds').text()) - 1) === -1) {
                $('#seconds').text('59');
                // minutes
                if ((Number($('#minutes').text()) - 1) === -1) {
                    $('#minutes').text('59');
                    // hours
                    if ((Number($('#hours').text()) - 1) === -1) {
                        $('#hours').text('24');
                        // days
                        if ((Number($('#days').text()) - 1) === -1) {
                            $('#days').text('0');
                        } else {
                            $('#days').text(`${Number($('#days').text()) - 1}`);
                        }
                    } else {
                        $('#hours').text(`${Number($('#hours').text()) - 1}`);
                    }
                } else {
                    $('#minutes').text(`${Number($('#minutes').text()) - 1}`);
                }
            } else {
                $('#seconds').text(`${Number($('#seconds').text()) - 1}`);
            }
            if (
                Number($('#seconds').text()) === 0 &&
                Number($('#minutes').text()) === 0 &&
                Number($('#hours').text()) === 0 &&
                Number($('#days').text()) === 0
            ) {
                $('#days').text('0');
                $('#hours').text('0');
                $('#minutes').text('0');
                $('#seconds').text('0');
                console.log('ended');
                clearInterval(countDown);
            }

        }, 1000);
    }
})




/** ==== zoom in ==== */
$(function () {
    $('.zoom-in').zoom();
})











/** ========= switching style sheets */
function setActiveStyleSheet(title) {
    console.log('=======================================');
    console.log('inside set active stylesheet');
    $('link[rel="stylesheet"][title]').map(function () {
        console.log($(this).prop('disabled'));
        $(this).prop('disabled', true);
        console.log($(this).prop('disabled'));
    })
    $(`link[rel="stylesheet"][title="${title}"]`).prop('disabled', false);
    console.log(title)
    console.log($(`link[rel="stylesheet"][title="${title}"]`).prop('disabled'))
}

function getActiveStyleSheet() {
    console.log('=======================================');
    console.log('inside get active stylesheet');
    var link = $('link[rel="stylesheet"][title]').filter(function () {
        return $(this).prop('disabled') === false
    })
    return $(link).attr('title');
}

function getPreferredStyleSheet() {
    console.log('=======================================');
    console.log('inside preferred');
    return 'light-theme';
}

function createCookie(name, value) {
    console.log('=======================================');
    console.log('inside create cookie');
    document.cookie = name + "=" + value + ";";
}

function readCookie(name) {
    console.log('=======================================');
    console.log('inside read cookie');
    var nameEQ = name + "=";
    var cookie = document.cookie.split(';').filter((item) => item.trim().startsWith(nameEQ));
    console.log(cookie[0])
    if (cookie.length) {
        console.log('The cookie "reader" exists (ES6)')
        console.log(cookie[0].substring(nameEQ.length + 1, cookie[0].length))
        return cookie[0].substring(nameEQ.length + 1, cookie[0].length) === "undefined" ? undefined
            : cookie[0].substring(nameEQ.length + 1, cookie[0].length);
    }
    return null;
}

window.onload = function (e) {
    console.log('=======================================');
    console.log('inside onload');
    var cookie = readCookie("stylesheet");
    var title = cookie ? cookie : getPreferredStyleSheet();
    setActiveStyleSheet(title);
}

window.onunload = function (e) {
    console.log('=======================================');
    console.log('inside onunload');
    var title = getActiveStyleSheet();
    console.log('/////////////////////////', title)
    createCookie("stylesheet", title);
}

var cookie = readCookie("stylesheet");
var title = cookie ? cookie : getPreferredStyleSheet();
setActiveStyleSheet(title);




$('#black').on('click', function (event) {
    setActiveStyleSheet('black-theme');
})
$('#light').on('click', function (event) {
    setActiveStyleSheet('light-theme');
})